import numpy as np

from sklearn import linear_model
from sklearn import preprocessing
from sklearn import model_selection

def fealect_score_sample(X, y):
    """ n_features x k FeaLect scores path
    for k-features level, each active feature gets a score of 1/k
    """
    alphas, active, coefs = linear_model.lars_path(X, y, method='lasso', verbose=True)
    k = (coefs != 0).sum(axis=0)
    max_features = k.max()

    # search for intervals on LARS path where exactly k features are active
    sel = []
    for nf in range(max_features):
        idx, = np.where(k == nf)
        if idx.size == 0:
            sel.append(0)
        else:
            sel.append(idx[0])

    c = coefs[:, sel]
    print(c.shape)
    weight = np.arange(max_features)
    weight[0] = 1 # avoid divide-by-zero when zero features are active anyways...

    return (c != 0) / weight

def fealect(X, y, n_iter=100):
    n_samples, n_features = X.shape
    feature_scores = np.zeros((n_features, n_features))
    std = preprocessing.StandardScaler()

    # accumulate feature scores across bootstrap samples
    for idx in range(n_iter):
        # bootstrap sample
        X_train, X_val, y_train, y_val = model_selection.train_test_split(X, y, test_size=0.2)

        scores = fealect_score_sample(std.fit_transform(X_train), y_train)
        _, n_active = scores.shape

        feature_scores[:,:n_active] += scores

    # reduce accumulated scores to mean score
    feature_scores /= n_iter

    # sum scores across model cardinality (k)
    return feature_scores.sum(axis=1)
