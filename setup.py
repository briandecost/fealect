from distutils.core import setup

setup(
    name='fealect',
    version='0.1',
    packages=['fealect',],
    license='NIST license',
    long_description=open('README.org').read(),
)
